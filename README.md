# Infrastructure

Project is prepared to put the application on a server based on RaspberryPi. 

Website [frontend] is available on AWS-Cloud -> http://ec2-18-193-69-233.eu-central-1.compute.amazonaws.com:8081/#/
API is available at http://83.230.14.77/bee (host pp-inf-back in container)  
DB is available only for backend, via local-IP address on RPi (host pp-inf-db in container) 

## Docker compose

Run
`docker-compose up -d` in root directory, to create db and backend.

