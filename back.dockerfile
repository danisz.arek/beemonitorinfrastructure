FROM node:13.14.0-buster-slim AS files

RUN apt-get update && \
    apt-get install -y ssh git

RUN mkdir -p ~/.ssh && \
    mkdir -p /root/build
RUN touch /root/.ssh/known_hosts && \
    ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
COPY keys/id_rsa /root/.ssh/

RUN chmod 0600 /root/.ssh/id_rsa

WORKDIR /root/build
RUN git clone git@gitlab.com:danisz.arek/beemonitor.git /root/build
COPY backend/application.properties /root/build/src/main/resources/

FROM qnerd/rpi-maven AS mvn
COPY --from=files /root/build ./root/build
RUN ls -alX && pwd
RUN mvn -f ./root/build/pom.xml compile war:war

FROM arm32v7/tomcat
COPY --from=mvn /data/./root/build/target/pp-app-pp.war .
RUN mv pp-app-pp.war $CATALINA_HOME/webapps/bee.war

WORKDIR $CATALINA_HOME
CMD ["catalina.sh", "run"]
